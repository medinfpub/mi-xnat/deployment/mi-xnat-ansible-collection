# xnat_openid_auth_plugin

## Parameters

### `xnat_openid_auth_plugin_state`

- default:

```
"enabled"

```

### `xnat_home`

- default:

```
"/data/xnat/home"

```

### `xnat_openid_auth_plugin_jar`

- default:

```
"base_url": "https://api.bitbucket.org/2.0/repositories/xnatx/openid-auth-plugin/downloads"
"name": "openid-auth-plugin"
"version": "1.2.0-SNAPSHOT-xpl"

```

### `xnat_openid_auth_plugin_provider_properties`

- default:

```
|
  auth.method=openid
  type=openid
  provider.id=openid
  visible=true
  auto.enabled=false
  auto.verified=true
  name=OpenID Authentication Provider
  disableUsernamePasswordLogin=false
  enabled=academiccloud
  siteUrl=https://xnat.miskatonic.edu
  preEstablishedRedirUri=/openid-login
  openid.academiccloud.clientId=
  openid.academiccloud.clientSecret=
  openid.academiccloud.accessTokenUri=
  openid.academiccloud.userAuthUri=
  openid.academiccloud.userInfoUri=
  openid.academiccloud.scopes=openid,profile,email
  openid.academiccloud.link=<p><b>Sign in with</b></p><a href="/openid-login?providerId=academiccloud"><button type="button" class="btn" style="width:100%">OpenID</button></a>
  openid.academiccloud.shouldFilterEmailDomains=false
  openid.academiccloud.forceUserCreate=true
  openid.academiccloud.userAutoEnabled=false
  openid.academiccloud.userAutoVerified=true
  openid.academiccloud.emailProperty=email
  openid.academiccloud.givenNameProperty=family_name
  openid.academiccloud.familyNameProperty=given_name
  openid.academiccloud.pkceEnabled=true
  openid.academiccloud.usernamePattern=[providerId]-[sub]

```

