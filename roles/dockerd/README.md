# dockerd

## Parameters

### `dockerd_state`

- default:

```
"enabled"

```

### `dockerd_packages`

- default:

```
- "docker-ce"
- "docker-ce-cli"
- "containerd.io"
- "docker-compose-plugin"

```

### `dockerd_port`

- default:

```
!!int "2376"

```

### `dockerd_local_ca_cert`

- default:

```
"{{ inventory_dir }}/group_vars/all/.ssl/ca/ca.pem"

```

### `dockerd_local_ssl_key`

- default:

```
"{{ inventory_dir }}/host_vars/{{ inventory_hostname }}/.ssl/server/key.pem"

```

### `dockerd_local_ssl_cert`

- default:

```
"{{ inventory_dir }}/host_vars/{{ inventory_hostname }}/.ssl/server/cert.pem"

```

