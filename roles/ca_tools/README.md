# ca_tools

## Parameters

### `ca_tools_state`

- default:

```
"enabled"

```

### `ca_tools_repo_url`

- default:

```
"https://gitlab.gwdg.de/medinfpub/mi-xnat/utils/ca-tools.git"

```

### `ca_tools_repo_path`

- default:

```
"/usr/local/src/mi-xnat/utils/ca-tools"

```

### `ca_tools_version`

- default:

```
"v1.0.0"

```

### `ca_tools_ca_key_dir`

- default:

```
"{{ inventory_dir }}/group_vars/all/.ssl/ca"

```

### `ca_tools_ca_cert_dir`

- default:

```
"{{ inventory_dir }}/group_vars/all/.ssl/ca"

```

### `ca_tools_ca_subject`

- default:

```
"{{ inventory_hostname }} CA"

```

### `ca_tools_ca_expire`

- default:

```
!!int "60"

```

### `ca_tools_ssl_size`

- default:

```
!!int "4096"

```

### `ca_tools_ssl_expire`

- default:

```
!!int "60"

```

